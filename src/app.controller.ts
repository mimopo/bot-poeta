import { Controller, Post, Body, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { Update } from 'telegram-typings';
import { WebhookAnswer } from './webhook-answer';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  receiveMessage(@Body() update: Update): WebhookAnswer {
    const message = update.message.text || '';
    const response = this.appService.getResponse(message);
    if (response) {
      return {
        method: 'sendMessage',
        chat_id: update.message.chat.id,
        text: response,
        reply_to_message_id: update.message.message_id,
      };
    }
    return;
  }

}
