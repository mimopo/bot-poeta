import { Injectable, HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class AppService {

  constructor(private http: HttpService) { }

  sendMessage(chatId: number, message: string): Observable<any> {
    return this.http.post('https://api.telegram.org/sendMessage' + process.env.TELEGRAM_TOKEN, {
      chat_id: chatId,
      text: message,
    });
  }

  getResponse(message: string): string {
    const text = message.toLowerCase();
    const words = text.split(' ');
    const lastWord = words[words.length - 1];
    if (lastWord.endsWith('inco') || lastWord.endsWith('5')) {
      return 'Por el culo te la hinco';
    }
    if (words.includes('cinco') || words.includes('5')) {
      return '¿Cuánto?';
    }
    return;
  }

}
