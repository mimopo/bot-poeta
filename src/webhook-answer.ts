export class WebhookAnswer {
    method: string;
    // tslint:disable-next-line:variable-name
    chat_id: number;
    text: string;
    // tslint:disable-next-line:variable-name
    reply_to_message_id: number;
}
